const path = require("path");
const HtmlWebPackPlugin = require("html-webpack-plugin");

const config = (_, argv) => {
    const development = argv.mode === "development";
    const htmlPlugin = new HtmlWebPackPlugin({ template: './index.html' });

    return {
        devtool: development ? "inline-source-map" : false,
        mode: development ? "development" : "production",
        entry: "./src/index",
        output: {
            path: path.resolve(__dirname, "public"),
            filename: development ? "bundle.js" : "bundle.[contenthash].js",
        },
        resolve: {
            extensions: [".js", ".jsx"]
        },
        module: {
            rules: [
                {
                    test: /\.jsx?$/,
                    loader: "babel-loader"
                }
            ],
        },
        plugins: [htmlPlugin],
    };
};

module.exports = config;
