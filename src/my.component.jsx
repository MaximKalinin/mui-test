import React from "react";
import Grid from "@material-ui/core/Grid";
import { useStyles } from "./styles";

export const MyComponent = () => {
    const classes = useStyles();
    return (
        <Grid container spacing={ 5 } className={ classes.root }>
            This element is without margin
        </Grid>
    );
};
